// Copyright 2021 by Fredrick Allan Grott
// BSD-style license that can be found in the LICENSE file.

class MyAppTag {
  String myApplicationTag = "Flutter Deep Dive";

  MyAppTag();
}
