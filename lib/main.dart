// Copyright 2021 by Fredrick Allan Grott
// BSD-style license that can be found in the LICENSE file.

import 'dart:async';
import 'dart:developer';

import 'package:ansicolor/ansicolor.dart';
import 'package:flutter/material.dart';
import 'package:flutter_deep_dive/domain/build_modes.dart';
import 'package:flutter_deep_dive/domain/my_log_setup.dart';
import 'package:flutter_deep_dive/domain/platform_targets.dart';

import 'package:flutter_deep_dive/domain/my_app_tag.dart';
import 'package:flutter_deep_dive/presentation/my_app.dart';

import 'package:flutter_deep_dive/services/service_locator.dart';
import 'package:logging/logging.dart';

// NOTE: The purpose of this Project is to set-up some common
//       boilerplate of typical apps so that a beginning
//       flutter app developer gets the right log, crash analytics, etc
//       from the very beginning coupled with the right widget testing,
//       goldens testing, instrumented testing, and bdd to effect
//       high-powered deep learning of flutter which then enables to
//       put in 1000 deliberate practice hours to equal the full
//       10,000 hours to become expert at flutter app development.
//
//       On purpose, I keep it agnostic of where you should hand
//       roll the cross-platform way to switch between MaterialApp and
//       CupertinoApp or the automatic platform_widgets way.
//
//       Service locators are being used as when we field inject
//       getit we than lose the ability to mock it and test it
//       an easy manner.

final Logger myMainLogger = Logger("myMain");

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  initPlatformState();
  myLogSetUp();
  setUpLocator();
  myMainLogger.info("init of main function completed");

  final String myTag = MyAppTag().myApplicationTag;

  ErrorWidget.builder = (FlutterErrorDetails details) {
    if (isInDebugMode) {
      return ErrorWidget(details.exception);
    }

    return Container(
      alignment: Alignment.center,
      child: const Text(
        'Error!',
        style: TextStyle(color: Colors.yellow),
        textDirection: TextDirection.ltr,
      ),
    );
  };

  FlutterError.onError = (FlutterErrorDetails details) async {
    if (isInDebugMode) {
      // In development mode simply print to console.
      FlutterError.dumpErrorToConsole(details);
    } else {
      // In production mode report to the application zone to report to
      // app exceptions provider. We do not need this in Profile mode.
      if (isInReleaseMode) {
        Zone.current.handleUncaughtError(details.exception, details.stack);
      }
    }
  };

  runZonedGuarded<Future<void>>(
    () async {
      runApp(MyApp());
    },
    (error, stackTrace) async {
      await _reportError(error, stackTrace);
    },
    zoneSpecification: ZoneSpecification(
      // Intercept all print calls
      print: (self, parent, zone, line) async {
        // Paint all logs with Cyan color
        final pen = AnsiPen()..cyan(bold: true);
        // Include a timestamp and the name of the App
        final messageToLog = "[${DateTime.now()}] $myTag $line";

        // Also print the message in the "Debug Console"
        parent.print(zone, pen(messageToLog));
      },
    ),
  );
}

Future<void> _reportError(dynamic error, dynamic stackTrace) async {
  log('Caught error: $error');
  // Errors thrown in development mode are unlikely to be interesting. You
  // check if you are running in dev mode using an assertion and omit send
  // the report.
  if (isInDebugMode) {
    log('$stackTrace');
    log('In dev mode. Not sending report to an app exceptions provider.');

    return;
  } else {
    // reporting error and stacktrace to app exceptions provider code goes here
    if (isInReleaseMode) {
      // code goes here
    }
  }
}


