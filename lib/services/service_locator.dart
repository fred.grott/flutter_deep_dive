// Copyright 2021 by Fredrick Allan Grott
// BSD-style license that can be found in the LICENSE file.

import 'package:flutter_deep_dive/domain/my_app_model.dart';

import 'package:get_it/get_it.dart';
import 'package:logging/logging.dart';

// Note, that when mixing both those with context and those
// dependencies that do not require a context those
// dependencies needed for the main function init
// block should be plain singletons as usually
// we do not get anything useful from unit testing
// such things as our logger set up, etc.
//
// Also note, that the service locator
// should always be initialized in the
// splash screen like:
// class SplashScreen extends StatelessWidget {
//    @override
//    Widget build(BuildContext context) {
//      return FutureBuilder<void>(
//        future: GetIt.allReady(),
//       builder: (context, snapshot) {
//          if (snapshot.hasData) {
// Navigate to main page (with replace)
//          } else if (snapshot.hasError) {
// Error handling
//         } else {
// Some pretty loading indicator
//          }
//       },
//     );
//  }
//
// the locator is still setup in main though
GetIt locator = GetIt.instance;

final Logger myServiceLocatorLogger = Logger("myServiceLocatorLogger");

Future<void> setUpLocator({bool test = false}) async {
  locator.registerLazySingleton<MyAppModel>(() => MyAppModel());

  myServiceLocatorLogger.info("service locators setup");

  //locator.registerLazySingleton<MyLoggerServices>(() => MyLoggerServices());

  //locator.registerLazySingleton<MyLoggingServices>(() => MyLoggingServices());
}
