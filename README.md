# Flutter_deep_dive

![deep dive](./images/git_project_card.png)

My thesis is that with the right flutter app project skeleton showing the right way to handle app state, dependencies, logging, etc will get both me and you to the 1000 to 2000 hours required to reach Flutter App Developer Stardom!

Included in this flutter application skeleton repo:

1. Proper Logging setup that is only that only logs in debug mode
2. Fast dependency injection that is faster than using the provider package and uses the service locator pattern.
3. Proper state management using the command patterns.
4. A project workflow semi-automated scripting system to reduce project work of generating project reports.
5. Proper integration testing setup.
6. Proper widget testing setup.

<!-- Shields -->
[![Twitter](https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white)](https://twitter.com/fredgrott)[![Xing](https://img.shields.io/badge/Xing-006567?style=for-the-badge&logo=xing&logoColor=white)](https://www.xing.com/profile/Fred_Grott/cv)[![keybase](https://img.shields.io/badge/Keybase-33A0FF?&style=for-the-badge&logo=keybase&logoColor=white)](https://keybase.io/fredgrott)[![gitlab](https://img.shields.io/badge/GitLab-330F63?style=for-the-badge&logo=gitlab&logoColor=white)](https://gitlab.com/fred.grott)[![medium](https://img.shields.io/badge/medium-%2312100E.svg?&style=for-the-badge&logo=medium&logoColor=white)](https://fredgrott.medium.com)[![linkedin](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/fredgrottstartupfluttermobileappdesigner/)![dart](https://img.shields.io/badge/dart-%230175C2.svg?&style=for-the-badge&logo=dart&logoColor=white)![flutter](https://img.shields.io/badge/Flutter%20-%2302569B.svg?&style=for-the-badge&logo=Flutter&logoColor=white)![vscode](https://img.shields.io/badge/VSCode-007ACC?&style=for-the-badge&logo=visual-studio-code&logoColor=white)![android studio](https://img.shields.io/badge/Android_Studio-3DDC84?&style=for-the-badge&logo=android-studio&logoColor=white)![markdown](https://img.shields.io/badge/Markdown-000000?&style=for-the-badge&logo=markdown&logoColor=white)![bsdlicense](https://img.shields.io/badge/-BSD_License-61DAFB?&logoColor=white&style=for-the-badge)

<!-- Using live badgen shield list from https://badgen.net/ -->
![Starrers](https://badgen.net/gitlab/stars/fred.grott/flutter_deep_dive)![Forks](https://badgen.net/gitlab/forks/fred.grott/flutter_deep_dive)![Open Issues](https://badgen.net/gitlab/open-issues/fred.grott/flutter_deep_dive)

1. [Flutter_deep_dive](Flutter deep dive)
   1. [Getting Started](# Getting Started)
   2. [Resources](# Resources)

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help to get started with Flutter, view our[online documentation](https://flutter.dev/docs), which offers tutorials, samples, guidance on mobile development, and a full API reference.

## Resources

Articles:

- Log Driven Learning Flutter(@medium) [Log Driven Learning Flutter](https://medium.com/p/log-driven-learning-flutter-d76b49b75a8c)

Books:

- Flutter Deep Dive, a Flutter Dev Book Series(website@github) [Flutter Deep Dive]()

Processes:

- Domain Driven Design(@wikipedia) [Domain Driven Design](https://en.wikipedia.org/wiki/Domain-driven_design)

- Acceptance Test Driven Development(@wikipedia) [Acceptance Test Driven Development](https://en.wikipedia.org/wiki/Acceptance_test%E2%80%93driven_development)

- Test Driven Development(@wikipedia) [Test Driven Development](https://en.wikipedia.org/wiki/Test-driven_development)

- Object Oriented Programming(@wikipedia) [Object Oriented Programming](https://en.wikipedia.org/wiki/Object-oriented_programming)

- Functional Programming(@wikipedia) [Functional Programming](https://en.wikipedia.org/wiki/Functional_programming)

Plugins:

- Ansicolor Plugin(@pub.dev) [Ansicolor](https://pub.dev/packages/ansicolor)

- Device Info Plus Plugin(@pub.dev) [Device Info Plus](https://pub.dev/packages/device_info_plus)

- Firebase Crashanalytics Plugin(@pub.dev) [Firebase Crashanalytics](https://pub.dev/packages/firebase_crashlytics)

- Flutter Command Plugin(@pub.dev) [Flutter Command](https://pub.dev/packages/flutter_command)

- Flutter Hooks Plugin(@pub.dev) [Flutter Hooks plugin](https://pub.dev/packages/flutter_hooks)

- Flutter Platform Widgets Plugin(@pub.dev) [Flutter Platform Widgets](https://pub.dev/packages/flutter_platform_widgets)

- GetIt Plugin(@pub.dev) [GetIt](https://pub.dev/packages/get_it)

- GetItMixin Plugin(@pub.dev) [GetIt mixin](https://pub.dev/packages/get_it_mixin)

- Golden Toolkit Plugin(@pub.dev) [Golden Toolkit](https://pub.dev/packages/golden_toolkit)

- Integration Testing Plugin(@pub.dev) [Integration Testing](https://pub.dev/packages/integration_test)

- Logging Plugin(@pub.dev) [Logging Plugin](https://pub.dev/packages/logging)

- Logging Appenders Plugin(@pub.dev) [Logging Appenders](https://pub.dev/packages/logging_appenders)

- Meta Plugin(@pub.dev) [Meta](https://pub.dev/packages/meta)

- Path Provider Plugin(@pub.dev) [Path Provider](https://pub.dev/packages/path_provider)

- Stack Trace plugin(@pub.dev) [Stack Trace](https://pub.dev/packages/stack_trace)
